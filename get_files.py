#!/bin/python
import sys
import os
import zipfile
import tarfile

#print 'Number of arguments:', len(sys.argv), 'arguments'
#print 'Argument List:', str(sys.argv)

pl = {'java': ['.java'],
      'c': ['.c','.h'],
      'c++': ['.cpp', '.hpp', '.cc', '.h'],
      'bash': ['.sh','.csh'],
      'others': ['.wsdl', '.l', '.y'],
      'all': ['java','c','c++','bash','others']
     }


if len(sys.argv) < 3 or not sys.argv[2] in pl.keys():
    print 'Run the script as follows:'
    print '   python %s <component> <pl>' % sys.argv[0]
    print 'where pl can be one of the following values'
    print '   %s' % ', '.join(pl.keys())
    sys.exit()

def prepare_files_to_check(location=''):
    """Prepare files to check"""
    proper_files=[]
    for root, dirs, files in os.walk(location):
        for file in files:
            if sys.argv[2] == 'all':
                es = [pl[x] for x in pl['all']]
                nes = list(set(reduce(lambda x,y: x+y, es)))
            else:
                nes = pl[sys.argv[2]]
            for x in nes:
                if file.endswith(x):
                    proper_files.append(os.path.join(root[2:], file))
    return proper_files

def delete_file(filename='', verbose=False):
    """Delete file to be recreated"""
    if os.path.isfile(filename):
        if verbose:
            print 'delete %s' % filename
        os.remove(filename)

def store_files_to_check(values, id_filename):
    """Store files to check in the release file"""
    for val in values:
        id_filename.write(val+'\n')

file_name = '%s.txt' % sys.argv[1]

delete_file(file_name)

values = prepare_files_to_check(location='.')
if not os.path.isfile(file_name):
    store_files = open(file_name, 'wa')

store_files_to_check(values, store_files)
store_files.close()


